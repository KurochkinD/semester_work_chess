import os
import time

import pygame
from pygame.locals import Rect

PIECE_IMAGES_PATH = os.path.join(os.path.pardir, "assets", "images", "pieces")
BOARD_IMAGE_PATH = os.path.join(os.path.pardir, "assets", "images", "boards", "b.png")
BUTTON_IMAGE_PATH = os.path.join(os.path.pardir, "assets", "images", "buttons")

from classes import Board, Queen, Button
from services import convert_pixels_to_position, convert_position_to_pixels, reformat_command
from client import connect_and_send_to_server
from settings import WIN_MESSAGE, PLAY_FOR_BLACK, DELIMITER, CHECK_MESSAGE


DISPLAY_HEIGHT = 900
DISPLAY_WIDHT = 650
LEFT_BORDER = 10
RECT_BORDER = 3

COLORS = {
    'white': (255, 255, 255),
    'black': (0, 0, 0),
    'aquamarine': (127, 255, 212),
    'powder_blue': (176, 224, 230),
    'tomato': (255, 69, 71),
    'check': (200, 53, 78)
}

PIECE_RECT_SIZE = (113, 113, 525, 525)

IMAGES = {
    'white': {
        'pawn': pygame.image.load(os.path.join(PIECE_IMAGES_PATH, "wP.png")),
        'bishop': pygame.image.load(os.path.join(PIECE_IMAGES_PATH, "wB.png")),
        'knight': pygame.image.load(os.path.join(PIECE_IMAGES_PATH, "wN.png")),
        'queen': pygame.image.load(os.path.join(PIECE_IMAGES_PATH, "wQ.png")),
        'king': pygame.image.load(os.path.join(PIECE_IMAGES_PATH, "wK.png")),
        'rook': pygame.image.load(os.path.join(PIECE_IMAGES_PATH, "wR.png"))},

    'black': {
        'pawn': pygame.image.load(os.path.join(PIECE_IMAGES_PATH, "bP.png")),
        'bishop': pygame.image.load(os.path.join(PIECE_IMAGES_PATH, "bB.png")),
        'knight': pygame.image.load(os.path.join(PIECE_IMAGES_PATH, "bN.png")),
        'queen': pygame.image.load(os.path.join(PIECE_IMAGES_PATH, "bQ.png")),
        'king': pygame.image.load(os.path.join(PIECE_IMAGES_PATH, "bK.png")),
        'rook': pygame.image.load(os.path.join(PIECE_IMAGES_PATH, "bR.png"))}
}


class Game:
    """
    player - 'white' or 'black'
    """

    def __init__(self, board: Board, player: str):
        pygame.init()

        self.board = board
        self.display = pygame.display.set_mode((670, 670))
        self.board_image = pygame.image.load(BOARD_IMAGE_PATH)
        self.clock = pygame.time.Clock()

        self.selected_piece = None
        self.possible_moves = []
        self.check_is_put = False
        self.checkmate_is_put = False

        self.player = player
        pygame.display.set_caption('pyChess')

    def play(self):
        while True:
            self.clock.tick(60)
            self.draw_board()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()

                if event.type == pygame.MOUSEBUTTONUP:
                    print(event)
                    returned_command = self.handle_click()
                    if returned_command:
                        self.draw_board()
                        return returned_command


    def draw_window(self):
        self.display.fill(COLORS['white'])
        self.draw_board()
        pygame.display.flip()

    def draw_board(self):
        self.display.blit(self.board_image, (0, 0))

        for row in self.board.board:
            for piece in row:
                if piece:
                    image_position = (
                        convert_position_to_pixels(piece.position)[0], convert_position_to_pixels(piece.position)[1]
                    )
                    piece_image = IMAGES[piece.color][piece.name]
                    self.display.blit(piece_image, image_position)

        if self.selected_piece:
            x, y = convert_position_to_pixels(self.selected_piece.position)
            pygame.draw.rect(self.display, COLORS['aquamarine'], Rect((x, y), (80, 80)), RECT_BORDER)

            for move in self.possible_moves:
                x, y = convert_position_to_pixels(move)
                pygame.draw.rect(self.display,
                                 COLORS['powder_blue'] if not self.board.board[move[0]][move[1]] else COLORS['tomato'],
                                 Rect((x, y), (80, 80)), RECT_BORDER)
        pygame.display.update()

    def handle_click(self):
        self.return_to_normal()
        if self.is_checkmate():
            self.draw_window()
            self.display_message('Checkmate!', (400, 200))
            self.display_message(f"{self.player} wins!", (400, 350))
            pygame.display.update()
            return WIN_MESSAGE
        x, y = pygame.mouse.get_pos()

        if not (20 < x < 655) or not (5 < y < 655):
            return
        selected_pos = convert_pixels_to_position(x, y)

        if not self.selected_piece:
            self.selected_piece = self.board.board[selected_pos[0]][selected_pos[1]]
            if self.selected_piece is None or self.selected_piece.color != self.player:
                self.selected_piece = None
                self.possible_moves = []
                return
            self.possible_moves = self.selected_piece.get_possible_positions()

        else:
            if selected_pos == self.selected_piece.position:
                self.selected_piece = None
                self.possible_moves = []

            elif selected_pos in self.possible_moves:
                to_return = self.selected_piece.position
                self.move_piece(self.selected_piece, selected_pos)

                if self.selected_piece.name == 'pawn' and (selected_pos[0] == 0 or selected_pos[0] == 7):
                    self.board.board[selected_pos[0]][selected_pos[1]] = \
                        Queen((selected_pos[0], selected_pos[1]), self.player)
                    self.board.board[selected_pos[0]][selected_pos[1]].change_possible_positions(self.board.board)

                self.is_check()
                self.selected_piece = None
                self.possible_moves = []
                return str(to_return)+ DELIMITER + str(selected_pos)

            elif self.board.board[selected_pos[0]][selected_pos[1]]:
                new_piece = self.board.board[selected_pos[0]][selected_pos[1]]
                if new_piece.color == self.player:
                    self.selected_piece = new_piece
                    self.possible_moves = self.selected_piece.get_possible_positions()
            else:
                self.selected_piece = None
                self.possible_moves = []

    def move_piece(self, piece, pos):
        self.board.board[piece.position[0]][piece.position[1]] = None
        piece.move(new_pos=pos)
        self.board.board[pos[0]][pos[1]] = piece
        self.board.change_all_positions()
        pygame.display.update()

    def display_message(self, text, point, fontsize=85):
        large_text = pygame.font.Font('freesansbold.ttf', fontsize)
        text_surface = large_text.render(text, True, COLORS['black'])
        text_rect = text_surface.get_rect()
        text_rect.center = point
        self.display.blit(text_surface, text_rect)

    def is_check(self):
        for pos_x, pos_y in self.selected_piece.get_possible_positions():
            piece = self.board.board[pos_x][pos_y]
            if piece and piece.name == "king" and piece.color != self.player:
                self.check_is_put = True
                return True
        return False

    def complete_opponent_command(self, opponent_command: list):
        for command in opponent_command:
            first_pos, second_pos = command
            first_pos = first_pos.split(" ")
            second_pos = second_pos.split(" ")
            first_pos[0], first_pos[1] = int(first_pos[0]), int(first_pos[1])
            second_pos[0], second_pos[1] = int(second_pos[0]), int(second_pos[1])

            piece = self.board.board[first_pos[0]][first_pos[1]]
            piece.move([second_pos[0], second_pos[1]])
            self.board.board[first_pos[0]][first_pos[1]] = None
            self.board.board[second_pos[0]][second_pos[1]] = piece

            if len(command) == 3:
                command = command[3]
                if command == CHECK_MESSAGE:
                    pass

        self.board.change_all_positions()
        pygame.display.update()


    def return_to_normal(self):
        self.check_is_put = False

    def is_checkmate(self):
        for x in range(8):
            for y in range(8):
                if self.board.board[x][y] is None or self.board.board[x][y].color != self.player:
                    continue
                white_piece = self.board.board[x][y]
                for pos_x, pos_y in white_piece.get_possible_positions():
                    piece = self.board.board[pos_x][pos_y]
                    if piece and piece.color != self.player and piece.name == "king":
                        self.checkmate_is_put = True
                        return True


class InitialScreen:
    def __init__(self):
        self.SCREEN_HEIGHT = 500
        self.SCREEN_WIDTH = 800

        self.screen = pygame.display.set_mode((self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
        pygame.display.set_caption("Chess")

        # load button images
        self.start_img = pygame.image.load(os.path.join(BUTTON_IMAGE_PATH, 'start_btn.png')).convert_alpha()
        self.exit_img = pygame.image.load(os.path.join(BUTTON_IMAGE_PATH, 'exit_btn.png')).convert_alpha()

        # create button instances
        self.start_button = Button(100, 200, self.start_img, 0.8)
        self.exit_button = Button(450, 200, self.exit_img, 0.8)

    # game loop
    def start(self):
        run = True
        while run:

            self.screen.fill((202, 228, 241))

            if self.start_button.draw(self.screen):
                main()
            if self.exit_button.draw(self.screen):
                run = False

            # event handler
            for event in pygame.event.get():
                # quit game
                if event.type == pygame.QUIT:
                    run = False

            pygame.display.update()

def main():
    # тут будет загрузочный экран
    while True:
        board = Board()
        board.init_fill()
        gen = connect_and_send_to_server()
        player = next(gen)
        if not player:
            raise SystemExit
        game = Game(board, player)
        game.draw_window()
        if player == PLAY_FOR_BLACK:
            opponent_command = next(gen)
            opponent_command = reformat_command(opponent_command)
            game.complete_opponent_command(opponent_command)
        try:
            while True:
                command_to_send = game.play()
                opponent_command = gen.send(command_to_send)
                opponent_command = reformat_command(opponent_command)
                game.complete_opponent_command(opponent_command)
        except StopIteration as e:
            print(e)


if __name__ == '__main__':
    start = InitialScreen()
    start.start()
