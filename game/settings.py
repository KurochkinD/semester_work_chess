PLAY_FOR_WHITE = "white"
PLAY_FOR_BLACK = "black"

MAX_COMMAND_SIZE = 512
LOSE_MESSAGE = "lose!!"
WIN_MESSAGE = "win!!"
CHECK_MESSAGE = "check_is_put"
HOST = "127.0.0.1"
PORT = 5555
DELIMITER = "-"

# ERRORS ---------------------------------------------------
SERVER_UNAVAILABLE_CODE = 10061
