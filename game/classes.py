from abc import ABC, abstractmethod
from typing import List, Tuple
import pygame

DEFAULT_POSITIONS = {
    'white': {
        'pawn': ((1, i) for i in range(8)),
        'rook': ((0, 0), (0, 7)),
        'knight': ((0, 1), (0, 6)),
        'bishop': ((0, 2), (0, 5)),
        'queen': ((0, 3),),
        'king': ((0, 4),)},

    'black': {
        'pawn': ((6, i) for i in range(8)),
        'bishop': ((7, 2), (7, 5)),
        'knight': ((7, 1), (7, 6)),
        'rook': ((7, 0), (7, 7)),
        'queen': ((7, 3),),
        'king': ((7, 4),)}
}


class Piece(ABC):
    '''
    position - два числа: [№ столбца, № строки]
    color - строкой: 'black' / 'white'
    '''

    def __init__(self, name: str = '', position: Tuple[int, int] = (), color: str = '', max_step: int = 1, ):
        self.name = name
        self.position = position
        self.color = color
        self.is_selected = False
        self.move_list = []
        self.max_step = max_step
        self.is_touched = False

    @abstractmethod
    def change_possible_positions(self, board: List) -> None:
        pass

    def get_possible_positions(self) -> List[Tuple[int, int]]:
        return self.move_list

    def move(self, new_pos: Tuple[int, int]):
        self.position = new_pos
        if not self.is_touched:
            self.is_touched = True


def add_possible_up_left_positions(piece: Piece, board: List) -> None:
    if piece.color == 'black':
        dest = -1
    else:
        dest = 1

    for i in range(1, piece.max_step + 1):
        i *= dest
        new_poses = [(piece.position[0] + i, piece.position[1] - i)]

        for pos in new_poses:
            if pos[0] in range(0, 8) and pos[1] in range(0, 8):
                if board[pos[0]][pos[1]]:
                    if board[pos[0]][pos[1]].color != piece.color:
                        piece.move_list.append(pos)
                    return

                piece.move_list.append(pos)


def add_possible_up_right_positions(piece: Piece, board: List) -> None:
    if piece.color == 'black':
        dest = -1
    else:
        dest = 1

    for i in range(1, piece.max_step + 1):
        i *= dest
        new_poses = [(piece.position[0] + i, piece.position[1] + i)]

        for pos in new_poses:
            if pos[0] in range(0, 8) and pos[1] in range(0, 8):
                if board[pos[0]][pos[1]]:
                    if board[pos[0]][pos[1]].color != piece.color:
                        piece.move_list.append(pos)
                    return

                piece.move_list.append(pos)


def add_possible_down_left_positions(piece: Piece, board: List) -> None:
    if piece.color == 'black':
        dest = -1
    else:
        dest = 1

    for i in range(1, piece.max_step + 1):
        i *= dest
        new_poses = [(piece.position[0] - i, piece.position[1] - i)]

        for pos in new_poses:
            if pos[0] in range(0, 8) and pos[1] in range(0, 8):
                if board[pos[0]][pos[1]]:
                    if board[pos[0]][pos[1]].color != piece.color:
                        piece.move_list.append(pos)
                    return

                piece.move_list.append(pos)


def add_possible_down_right_positions(piece: Piece, board: List) -> None:
    if piece.color == 'black':
        dest = -1
    else:
        dest = 1

    for i in range(1, piece.max_step + 1):
        i *= dest

        new_poses = [(piece.position[0] - i, piece.position[1] + i)]

        for pos in new_poses:
            if pos[0] in range(0, 8) and pos[1] in range(0, 8):
                if board[pos[0]][pos[1]]:
                    if board[pos[0]][pos[1]].color != piece.color:
                        piece.move_list.append(pos)
                    return

                piece.move_list.append(pos)


def add_possible_forward_positions(piece: Piece, board: List) -> None:
    if piece.color == 'black':
        dest = -1
    else:
        dest = 1

    for i in range(1, piece.max_step + 1):
        i *= dest
        if 0 <= piece.position[0] + i <= 7:
            if board[piece.position[0] + i][piece.position[1]]:
                if board[piece.position[0] + i][piece.position[1]].color != piece.color:
                    piece.move_list.append((piece.position[0] + i, piece.position[1]))
                return
            piece.move_list.append((piece.position[0] + i, piece.position[1]))


def add_possible_left_positions(piece: Piece, board: List) -> None:
    if piece.color == 'black':
        dest = -1
    else:
        dest = 1

    for i in range(1, piece.max_step + 1):
        i *= dest
        if 7 >= piece.position[1] - i >= 0:
            if board[piece.position[0]][piece.position[1] - i]:
                if board[piece.position[0]][piece.position[1] - i].color != piece.color:
                    piece.move_list.append((piece.position[0], piece.position[1] - i))
                return
            piece.move_list.append((piece.position[0], piece.position[1] - i))


def add_possible_right_positions(piece: Piece, board: List) -> None:
    if piece.color == 'black':
        dest = -1
    else:
        dest = 1

    for i in range(1, piece.max_step + 1):
        i *= dest
        if 0 <= piece.position[1] + i <= 7:
            if board[piece.position[0]][piece.position[1] + i]:
                if board[piece.position[0]][piece.position[1] + i].color != piece.color:
                    piece.move_list.append((piece.position[0], piece.position[1] + i))
                return
            piece.move_list.append((piece.position[0], piece.position[1] + i))


def add_possible_back_positions(piece: Piece, board: List) -> None:
    if piece.color == 'black':
        dest = -1
    else:
        dest = 1

    for i in range(1, piece.max_step + 1):
        i *= dest
        if 7 >= piece.position[0] - i >= 0:
            if board[piece.position[0] - i][piece.position[1]]:
                if board[piece.position[0] - i][piece.position[1]].color != piece.color:
                    piece.move_list.append((piece.position[0] - i, piece.position[1]))
                return
            piece.move_list.append((piece.position[0] - i, piece.position[1]))


class King(Piece):
    def __init__(self, position: Tuple[int, int] = (), color: str = ''):
        super().__init__(name='king', max_step=1, color=color, position=position)

    def change_possible_positions(self, board: List) -> List[Tuple[int, int]]:
        self.move_list = []
        add_possible_back_positions(self, board)
        add_possible_forward_positions(self, board)
        add_possible_up_left_positions(self, board)
        add_possible_up_right_positions(self, board)
        add_possible_down_right_positions(self, board)
        add_possible_down_left_positions(self, board)
        add_possible_left_positions(self, board)
        add_possible_right_positions(self, board)

        if not self.is_touched:
            if self.color == 'white':
                if (board[0][1] is None and board[0][2] is None and board[0][3] is None) and not board[0][0].is_touched:
                    self.move_list.append((0, 2))

                if (board[0][6] is None and board[0][5] is None) and not board[0][7].is_touched:
                    self.move_list.append((0, 6))

            else:
                if (board[7][1] is None and board[7][2] is None and board[7][3] is None) and not board[7][0].is_touched:
                    self.move_list.append((7, 2))

                if (board[7][6] is None and board[7][5] is None) and not board[7][7].is_touched:
                    self.move_list.append((7, 6))

        return self.move_list


class Queen(Piece):
    def __init__(self, position: Tuple[int, int] = (), color: str = ''):
        super().__init__(name='queen', max_step=7, color=color, position=position)

    def change_possible_positions(self, board: List) -> List[Tuple[int, int]]:
        self.move_list = []
        add_possible_back_positions(self, board)
        add_possible_forward_positions(self, board)
        add_possible_up_left_positions(self, board)
        add_possible_up_right_positions(self, board)
        add_possible_down_right_positions(self, board)
        add_possible_down_left_positions(self, board)
        add_possible_left_positions(self, board)
        add_possible_right_positions(self, board)

        return self.move_list


class Bishop(Piece):
    def __init__(self, position: Tuple[int, int] = (), color: str = ''):
        super().__init__(name='bishop', max_step=7, color=color, position=position)

    def change_possible_positions(self, board: List) -> List[Tuple[int, int]]:
        self.move_list = []
        add_possible_up_left_positions(self, board)
        add_possible_up_right_positions(self, board)
        add_possible_down_right_positions(self, board)
        add_possible_down_left_positions(self, board)

        return self.move_list


class Knight(Piece):
    def __init__(self, position: Tuple[int, int] = (), color: str = ''):
        super().__init__(name='knight', color=color, position=position)

    def change_possible_positions(self, board: List) -> List[Tuple[int, int]]:
        self.move_list = []
        new_poses = [(self.position[0] + 2, self.position[1] + 1),
                     (self.position[0] + 2, self.position[1] - 1),
                     (self.position[0] - 2, self.position[1] + 1),
                     (self.position[0] - 2, self.position[1] - 1),
                     (self.position[0] + 1, self.position[1] + 2),
                     (self.position[0] - 1, self.position[1] + 2),
                     (self.position[0] + 1, self.position[1] - 2),
                     (self.position[0] - 1, self.position[1] - 2)]

        for pos in new_poses:
            if pos[0] in range(0, 8) and pos[1] in range(0, 8):
                if board[pos[0]][pos[1]]:
                    if board[pos[0]][pos[1]].color != self.color:
                        self.move_list.append(pos)
                    continue
                self.move_list.append(pos)

        return self.move_list


class Rook(Piece):
    def __init__(self, position: Tuple[int, int] = (), color: str = ''):
        super().__init__(name='rook', max_step=7, color=color, position=position)

    def change_possible_positions(self, board: List) -> List[Tuple[int, int]]:
        self.move_list = []
        add_possible_forward_positions(self, board)
        add_possible_left_positions(self, board)
        add_possible_right_positions(self, board)
        add_possible_back_positions(self, board)

        return self.move_list


class Pawn(Piece):
    def __init__(self, position: Tuple[int, int] = (), color: str = ''):
        super().__init__(name='pawn', max_step=1, color=color, position=position)

    def change_possible_positions(self, board) -> List[Tuple[int, int]]:
        self.move_list = []
        if self.color == 'black':
            dest = -1
        else:
            dest = 1

        for i in range(1, self.max_step + 1):
            i *= dest
            if 0 <= self.position[0] + i <= 7:
                if board[self.position[0] + i][self.position[1]]:
                    break
                self.move_list.append((self.position[0] + i, self.position[1]))

        for i in range(1, self.max_step + 1):
            i *= dest
            new_poses = [(self.position[0] + i, self.position[1] - i)]

            for pos in new_poses:
                if pos[0] in range(0, 8) and pos[1] in range(0, 8):
                    if board[pos[0]][pos[1]]:
                        if board[pos[0]][pos[1]].color != self.color:
                            self.move_list.append(pos)

        for i in range(1, self.max_step + 1):
            i *= dest
            new_poses = [(self.position[0] + i, self.position[1] + i)]

            for pos in new_poses:
                if pos[0] in range(0, 8) and pos[1] in range(0, 8):
                    if board[pos[0]][pos[1]]:
                        if board[pos[0]][pos[1]].color != self.color:
                            self.move_list.append(pos)

        if self.color == 'white' and self.position[0] == 1:
            self.move_list.append((self.position[0] + 2, self.position[1]))
        elif self.color == 'black' and self.position[0] == 6:
            self.move_list.append((self.position[0] - 2, self.position[1]))
        return self.move_list


CLASSES = {
    'pawn': Pawn,
    'queen': Queen,
    'king': King,
    'rook': Rook,
    'bishop': Bishop,
    'knight': Knight
}


class Board:
    def __init__(self):
        self.board = [[None for i in range(8)] for i in range(8)]

    def init_fill(self):
        for color in ['white', 'black']:
            pieces = DEFAULT_POSITIONS[color]
            for piece in pieces:
                poses = pieces[piece]
                for pos in poses:
                    self.board[pos[0]][pos[1]] = CLASSES[piece](pos, color)
        self.change_all_positions()

    def change_all_positions(self):
        for x in range(8):
            for y in range(8):
                piece = self.board[x][y]
                if not piece:
                    continue
                piece.change_possible_positions(self.board)


class Button:
    def __init__(self, x, y, image, scale):
        width = image.get_width()
        height = image.get_height()
        self.image = pygame.transform.scale(image, (int(width * scale), int(height * scale)))
        self.rect = self.image.get_rect()
        self.rect.topleft = (x, y)
        self.clicked = False

    def draw(self, surface):
        action = False
        pos = pygame.mouse.get_pos()

        if self.rect.collidepoint(pos):
            if pygame.mouse.get_pressed()[0] == 1 and self.clicked == False:
                self.clicked = True
                action = True

        if pygame.mouse.get_pressed()[0] == 0:
            self.clicked = False

        surface.blit(self.image, (self.rect.x, self.rect.y))

        return action


if __name__ == '__main__':
    board_ = Board()
    board_.init_fill()
    for i in board_.board:
        print(i)
        print('------')
