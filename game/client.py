import socket
from settings import HOST, PORT, MAX_COMMAND_SIZE, WIN_MESSAGE, LOSE_MESSAGE, \
    PLAY_FOR_WHITE, PLAY_FOR_BLACK, SERVER_UNAVAILABLE_CODE


def routine(generator):
    def wrapper(*args, **kwargs):
        gen = generator(*args, **kwargs)
        next(gen)
        return gen

    wrapper.__name__ = generator.__name__
    return wrapper


def connect_and_send_to_server() -> str:
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    i = 0
    while True:
        try:
            i += 1
            print(i)
            while True:
                print("try to connect to server")
                client_socket.connect((HOST, PORT))
                break

            response = client_socket.recv(MAX_COMMAND_SIZE).decode("utf-8")
            print(response)
            while response != PLAY_FOR_WHITE and response != PLAY_FOR_BLACK:
                response = client_socket.recv(MAX_COMMAND_SIZE).decode("utf-8")
                print(response)

            if response == PLAY_FOR_WHITE:
                message = yield response
                client_socket.send(message.encode())
            else:
                yield response

            while True:
                response = client_socket.recv(MAX_COMMAND_SIZE).decode("utf-8")
                if response == WIN_MESSAGE or response == LOSE_MESSAGE:
                    return response
                message = yield response
                client_socket.send(message.encode())

        except socket.error as e:
            print(e)
            if e.errno == SERVER_UNAVAILABLE_CODE:
                repeat = yield "We need to repeat"
                if repeat != "repeat":
                    break


# python game/server.py
# python game/client.py
if __name__ == "__main__":
    gen = connect_and_send_to_server()
    print(dir(gen))
    gen.send("repeat")
