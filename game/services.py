from typing import Tuple
from settings import DELIMITER


def convert_pixels_to_position(x, y) -> Tuple[int, int]:
    return 7 - y // 80, (x - 25) // 80

def convert_position_to_pixels(pos : Tuple[int, int]):
    return pos[1] * 80 + 25, (7 - pos[0]) * 80 + 10

def reformat_command(command: str):
    command = command.replace("(", "").replace(")", "").replace(",", "")
    command = command.split(DELIMITER)
    if len(command) == 4:
        command = ([command[0], command[1]], [command[2], command[3]])
    if len(command) == 3:
        command = (command[1], command[2], command[0])
    elif len(command) == 2:
        command = ([command[0], command[1]],)
    return command
