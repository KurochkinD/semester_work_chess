import socket
import random
from threading import Thread

from settings import HOST, PORT, MAX_COMMAND_SIZE, WIN_MESSAGE, LOSE_MESSAGE, \
    PLAY_FOR_WHITE, PLAY_FOR_BLACK


AMOUNT_IN_ROOM = 2

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen(5)
rooms = []
threads = {}
user_amount = 0

print("server is running")


def chatting_in_room(connection1, address1, connection2, address2):
    if random.randint(0, 1):
        main_connection = connection2
        main_address = address2
        secondary_connection = connection1
        secondary_address = address1
    else:
        main_connection = connection1
        main_address = address1
        secondary_connection = connection2
        secondary_address = address2

    print(f"room \n     first address - {secondary_connection}, second one - {main_connection}")
    print(f"    first connection - {secondary_connection}, second connection - {main_connection}")

    secondary_connection.send(PLAY_FOR_WHITE.encode())
    main_connection.send(PLAY_FOR_BLACK.encode())

    while True:
        main_connection, secondary_connection = secondary_connection, main_connection
        main_address, secondary_address = secondary_address, main_address

        data = main_connection.recv(MAX_COMMAND_SIZE).decode("utf-8")
        if WIN_MESSAGE in data:
            secondary_connection.send(LOSE_MESSAGE.encode())
            break
        print(main_address, "send - ", data)
        secondary_connection.send(f"{data}".encode())

    main_connection.close()
    secondary_connection.close()
    print(f"room {address1}{address2} closed")
    del threads[address1 + address2]


while True:
    conn, address = server_socket.accept()
    if not conn:
        continue

    user_amount += 1
    rooms.extend((conn, address))
    print(f"New user on address{address}")
    if user_amount % AMOUNT_IN_ROOM != 0:
        conn.send("Waiting for user(s)".encode())
        continue

    first_connection = rooms[0]
    first_address = rooms[1]
    second_connection = rooms[2]
    second_address = rooms[3]
    first_connection.send("Second user found \nCreating new room...".encode())
    second_connection.send("Second user found \nCreating new room...".encode())
    thread = Thread(target=chatting_in_room,
                    args=(first_connection, first_address, second_connection, second_address))
    threads[first_address + second_address] = thread
    thread.start()
    rooms = []
    user_amount = 0
