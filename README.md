# PyChess

### Chess made with [pygame](https://www.pygame.org/docs/) and [sockets](https://docs.python.org/3/library/socket.html) & [threading](https://docs.python.org/3/library/threading.html)

![pyChess](assets/screenshots/game_start.png?raw=true "screen of gamestart")
## Requirements

* Python 3.9+

## Installation and venv creating

```sh
$ git clone https://gitlab.com/KurochkinD/semester_work_chess.git
$ python -m venv .venv
$ pip install poetry
$ poetry shell
$ poetry install
```

## Run

```sh
$ python server.py
```

```sh
$ python game.py
```

*and one more from another terminal:*

```sh
$ python game.py
```